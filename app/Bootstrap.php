<?php
date_default_timezone_set('Europe/Oslo');
require 'vendor/autoload.php';

$app = new \Slim\Slim(array(
  'mode' => 'development',
  'debug' => true
));

/* Set up a config container. In this example, we put in a configuration file. It can also be an instance of a class */
$app->container->set('config', function () {
  return json_decode(file_get_contents('app/configuration.json'), true);
});

// A database connection can be useful in a container
/* $app->container->singleton('db', function() {
  return new PDO('mysql:host=localhost;dbname=restapi;charset=utf8', 'root', '');
}); 
$app->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$app->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
*/

// If you need Basic Auth
/* $app->add(new \Slim\Middleware\HttpBasicAuthentication([
  "path" => "/",
  "realm" => "protected",
  "passthrough" => ["/auth"], // Allow /auth to be access without Basic Auth
  "users" => [
    "root" => "toor"
  ]
])); */

/**
 * Or we can use JWT. If using Apache, add 
 * RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
 * to the .htaccess file. Otherwise PHP won't have access to Authorization Bearer header
 */

/* $app->add(new Tuupola\Middleware\JwtAuthentication([
  "path" => ["/api", "/admin"],
  "ignore" => ["/api/token", "/admin/ping"],
  "secret" => "supersecretkeyyoushouldnotcommittogithub"
])); */

// If using JWT, you want a way to encode and decode a token
/* function verifyExp ($exp) {
  return time() > $exp;
}

$app->get('/jwt', function () use ($app) {
  $user = [
    'name' => 'Paven', // or some other things you need to store
    'iat' => time(), // Time the token was issued
    'exp' => time() + (60 * 5) // Expire token in 5 minutes
  ];
  $token = JWT::encode($user, 'supersecretkeyyoushouldnotcommittogithub');
  // If you have a token, for instance in a header
  // $passedToken = $app->request->headers->get('Authorization');
  $passedToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiUGF2ZW4iLCJpYXQiOjE1NTQwNjkzNTUsImV4cCI6MTU1NDA2OTY1NX0.QmVyv2D0kGRtMAbJGagZpACZZc7HQ0jfTNat8m_C8u8';
  $validate = JWT::decode($passedToken, 'supersecretkeyyoushouldnotcommittogithub');
  $expired = verifyExp($validate->exp);
  $app->render(200, [
    'data' => $user,
    'token' => $token,
    'valid' => $validate,
    'expired' => $expired,
    'msg' => 'Try decoding this token at https://jwt.io/'
  ]);
}); */




$app->add(new \CorsMiddleware());
$app->add(new \JsonApiMiddleware());
$app->view(new \JsonApiView());
require('app/Routes/Routes.php');
$app->run();