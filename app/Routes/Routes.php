<?php
$app->get('/', function () use ($app) {
  $app->render(200, array('msg' => 'Ok'));
});
/**
 * These are example routes.
 * You can choose between 
 * $app->get()
 * $app->post()
 * $app->delete()
 * $app->put()
 * $app->patch()
 */
$app->group('/example', function () use ($app) { // You need to curry $app in the scope
  $app->get('', 'ExampleController:NoArguments');
  $app->get('/:user', 'ExampleController:GetArgument');
  $app->post('', 'ExampleController:PostArgument');
});

/**
 * You don't need to make a new Class for everything. Need a route just to check things?
 */
$app->get('/check', function () use ($app) {
  // Your logic
  $app->render(200, ['data' => 'Some data']);
});

/** Custom Routes
 * One route, multiple methods?
 */
$app->map('/foo/bar', function() {
  echo "I respond to multiple HTTP methods!";
})->via('GET', 'POST');

/**
 * Parameters
 */
$app->get('/books/:one/:two', function ($one, $two) {
  echo "The first parameter is " . $one;
  echo "The second parameter is " . $two;
});

$app->get('/conf', 'ExampleController:AccessContainer');