<?php
/**
 * Base Controller. You can write functions here that all other controllers inherit. 
 */

class BaseController {
  public $app = null;
  public function __construct() {
    $this->app = \Slim\Slim::getInstance();
  }
}
