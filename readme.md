# Install
```
composer install
```

# Url rewriting
For Apache
.htaccess
```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [QSA,L]
```

Without url rewriting, you can access your api with
/index.php/foo
